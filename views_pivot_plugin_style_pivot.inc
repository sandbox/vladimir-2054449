<?php
class views_pivot_plugin_style_pivot extends views_plugin_style {

  function option_definition() {
    $options = parent::option_definition();
    $fields = '';
    $counter = 0;
    if (!empty($this->view)) {
      if (!empty($this->view->field)) {
        $keys = array_keys($this->view->field);
        foreach ($keys as $value) {
          $fields[$counter]['name'] = $value;
          $fields[$counter]['type'] = 'string';
          $fields[$counter]['filterable'] = false;
          $fields[$counter]['columnLabelable'] = false;
          $fields[$counter]['rowLabelable'] = true;
          $fields[$counter]['summarizable'] = false;
          $counter++;
        }
        $options['pivot-default'] = array(
          'default' => 'pivot',
          'contains' => $fields,
        );
      }
    }
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $module_path = drupal_get_path('module', 'views_pivot');
    $form['fields_info'] = array(
      '#type' => 'markup',
      '#markup' => t('<strong>Please change the type of the fields if it necessary!</strong>')
    );
    $field_labels = $this->display->handler->get_field_labels(TRUE);
    foreach ($field_labels as $key => $label) {
      $form['fields'][$key] = array(
        '#type' => 'fieldset',
        '#title' => t($label),
        '#collapsible' => FALSE,
        '#attributes' => array(
          'id' => array('pivot-set-' . $key),
          'class' => array('pivot-set'),
        ),
      );
      $form['fields'][$key]['type'] = array(
        '#type' => 'select',
        '#title' => t('Type of the field'),
        '#options' => array(
          'string' => t('string'),
          'integer' => t('integer'),
          'float' => t('float'),
          'date' => t('date'),
        ),
        '#default_value' => $this->options['fields'][$key]['type'],
        '#attributes' => array(
          'class' => array('pivot-field-type'),
        ),
      );
      $form['fields'][$key]['filterable'] = array(
        '#type' => 'checkbox', 
        '#title' => t('Filterable'),
        '#default_value' => $this->options['fields'][$key]['filterable'],
        '#attributes' => array(
          'class' => array('pivot-filterable')
        ),
      );
      $form['fields'][$key]['filter_type'] = array(
        '#type' => 'select',
        '#title' => t('Type of the filter'),
        '#options' => array(
          'none' => t('none'),
          'regexp' => t('regexp'),
          'multiselect' => t('multiselect'),
        ),
        '#attributes' => array(
          'class' => array('pivot-filter_type')
        ),
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[fields][' . $key . '][filterable]"]' => array('checked' => TRUE),
          ),
         ),
        '#default_value' => $this->options['fields'][$key]['filter_type']
      );
      $form['fields'][$key]['columnLabelable'] = array(
        '#type' => 'checkbox', 
        '#title' => t('columnLabelable'),
        '#default_value' => $this->options['fields'][$key]['columnLabelable'],
      );
      $form['fields'][$key]['rowLabelable'] = array(
        '#type' => 'checkbox', 
        '#title' => t('rowLabelable'),
        '#default_value' => $this->options['fields'][$key]['rowLabelable'],
      );
      $form['fields'][$key]['summarizable'] = array(
        '#type' => 'checkbox', 
        '#title' => t('summarizable'),
        '#default_value' => $this->options['fields'][$key]['summarizable'],
        '#attributes' => array(
          'class' => array('pivot-summarizable')
        ),
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[fields][' . $key . '][type]"]' => array('value' => 'integer'),
          ),
        ),
      );
    }
  }
  
  function render() {
    $options = $this->options;
    $headers = array_keys($this->view->field);
    $fields = $this->render_fields($this->view->result);
    $fields_values = array();
    foreach ($fields as $field) {
      $fields_values[] = array_values($field);
    }
    array_unshift($fields_values, $headers);
    $json_string = json_encode($fields_values);   
    $fields_settings = array();
    $counter = 0;
    foreach ($options['fields'] as $field_name => $field_value) {
      $fields_settings[$counter] = array('name' => $field_name);
      foreach ($field_value as $key => $value) {
        switch ($key) {
          case 'type':
            $fields_settings[$counter]['type'] = $value;
            break;
          case 'filterable': 
            if ($value == 1) {
              $fields_settings[$counter]['filterable'] = true;
            }
            else {
              $fields_settings[$counter]['filterable'] = false;
            }
            break;
          case 'filter_type':
            if ($value == 'none' || $field_value['filterable'] == 0) {
              continue;
            }
            else {
              $fields_settings[$counter]['filterType'] = $value;
            }
            break;
          case 'columnLabelable': 
            if ($value == 1) {
              $fields_settings[$counter]['columnLabelable'] = true;
            }
            else {
              $fields_settings[$counter]['columnLabelable'] = false;
            }
            break;
          case 'rowLabelable': 
            if ($value == 1) {
              $fields_settings[$counter]['rowLabelable'] = true;
            }
            else {
              $fields_settings[$counter]['rowLabelable'] = false;
            }
            break;
          case 'summarizable': 
            if ($value == 1) {
              $fields_settings[$counter]['summarizable'] = true;
            }
            else {
              $fields_settings[$counter]['summarizable'] = false;
            }
            break;
        }
      }
      $counter++;
    }

    $defs = $this->option_definition();
    $default_fileds_settings = $defs['pivot-default']['contains'];
    foreach ($default_fileds_settings as $key_def_set => $value_def_set) {
      foreach ($fields_settings as $key_field_set => $value_field_set) {
        if ($value_def_set['name'] == $value_field_set['name']) {
          $default_fileds_settings[$key_def_set] = $fields_settings[$key_field_set];
        }
      }
    }
    
    $full_config = json_encode($default_fileds_settings);
    $output = '';
    $output = '<div id="pivot-container"></div><div id="results"></div>';
    
    drupal_add_js(array('json_string' => $json_string), 'setting');
    drupal_add_js(array('full_config' => $full_config), 'setting');
    $module_path = drupal_get_path('module', 'views_pivot');
    drupal_add_js($module_path . '/js/pivot.js');
    drupal_add_js($module_path . '/js/jquery_pivot.js');
    drupal_add_js($module_path . '/js/table_init.js');
      
    return $output;
  }
}