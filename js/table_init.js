(function ($) {
  Drupal.behaviors.pivot_table_init = {
    attach : function(context, settings) {
      var json_fields = Drupal.settings.json_string;
      var fields_settings = $.parseJSON(Drupal.settings.full_config);
      $('#pivot-container', context).pivot_display('setup', {json:json_fields, fields:fields_settings});
    }
  };
})(jQuery);