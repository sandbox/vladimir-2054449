<?php
/**
 * Implementation of hook_views_plugins().
 */
function views_pivot_views_plugins() {
  return array(
    'module' => 'views_pivot',
    'style' => array(
      'views_pivot_style' => array(
        'title' => t('Pivot-table'),
        'handler' => 'views_pivot_plugin_style_pivot',
        'path' => drupal_get_path('module', 'views_pivot'),
        'type' => 'normal',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'even empty' => FALSE,
      ),
    ),
  );
}